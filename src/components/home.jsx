import React from 'react';
import { connect } from 'react-redux';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import '../App.css';
import Image from 'react-bootstrap/Image';
import Rating from '@material-ui/lab/Rating';
import HeaderMain from './HeaderMain.jsx';
import { withRouter } from 'react-router-dom';

class Home extends React.Component {

    viewProduct = (productId) => {
        this.props.history.push('/viewproduct/'+ productId)
    }

    render() {
        let productViews = [];
        for (let i = 0; i < this.props.products.length; i++) {
            productViews.push(
                <div key={i} className="col-md-3 cols-sm-3 margin-top-list">
                    <Card style={{ width: '18rem' }}>
                        <Image variant="top" className="product-img" src={`./${this.props.products[i].imageUrl}`} />
                        <Card.Body>
                            <Card.Title>{this.props.products[i].productName}</Card.Title>
                            <Card.Text>
                                Price : &#2352;&nbsp;{this.props.products[i].price}<br/>
                                <Rating name="read-only" value={this.props.products[i].rating} readOnly />
                            </Card.Text>
                            <Button variant="outline-primary" onClick={(e) => {this.viewProduct(this.props.products[i].productId)}}>View</Button>
                        </Card.Body>
                    </Card>
                </div>
            );
        }
        return (
            <React.Fragment>
                <HeaderMain />
                {this.props.products.length > 0 ? <div className="row">{productViews}</div> : <div className="row">
                    <div className="col-md-12 col-sm-12 text-center">
                        No Products forund
                        </div>
                </div>}
            </React.Fragment>
        )
    }
}

function mapStateToProps(state, props) {
    return {
        products: state.loginLogoutReducer.products
    };
}

export default withRouter(connect(mapStateToProps)(Home));