import React from 'react';
import { connect } from 'react-redux';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import '../App.css';
import HeaderMain from './HeaderMain.jsx';

class SignUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isvalid: false,
            message: '',
            userpasswordMatch: false,
            username: '',
            password: '',
            confirmpassword: '',
            email: '',
            mobile: 0

        }
        this.handleLogin = this.handleLogin.bind(this)
    }

    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    handleLogin(event) {
        event.preventDefault();
        const login = event.currentTarget;
        if (login.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
            this.setState({ message: "" })
            this.setState({ isvalid: true });
        } else {
        }
    }

    signUp = () => {
        setTimeout(() => {
            if (this.state.password !== '' && this.state.confirmpassword !== '' ) {
                if (this.state.password === this.state.confirmpassword) {
                    this.setState({ message: 'Successfully registered. You can login now' });
                } else {
                    this.setState({ message: 'Paswword and confirm password are not same' });
                }
            }
        }, 1);
    }

    loginBack = () => {
        this.props.history.push('/');
    }

    render() {
        const { isvalid } = this.state;
        return (
            <React.Fragment>
                <HeaderMain />
                <div className="row">
                    <div className="col-md-4 col-sm-4 offset-md-4">
                        <Card>
                            <CardContent>
                                <Form noValidate validated={isvalid}
                                    onSubmit={e => this.handleLogin(e)}>
                                    <div className="text-center" style={{ padding: "10px" }}>
                                        <h3> Login </h3><br />
                                        <Form.Group className="text-left">
                                            <label> User Name </label>
                                            <Form.Control type="text" style={{ backgroundColor: "##C0C0C0" }}
                                                name="username" placeholder="Enter Name" value={this.state.username} onChange={(e) => {this.onChange(e)}}
                                                required />
                                            <Form.Control.Feedback type="invalid">
                                                User Name is required
                                            </Form.Control.Feedback>
                                        </Form.Group>
                                        <br />
                                        <Form.Group className="text-left">
                                            <label> Password </label>
                                            <Form.Control type="password" style={{ backgroundColor: "##C0C0C0" }}
                                                name="password" placeholder="Enter Name" value={this.state.password} onChange={(e) => {this.onChange(e)}}
                                                required />
                                            <Form.Control.Feedback type="invalid">
                                                Password is required
                                            </Form.Control.Feedback>
                                        </Form.Group>
                                        <br />
                                        <Form.Group className="text-left">
                                            <label> Confirm Password </label>
                                            <Form.Control type="password" style={{ backgroundColor: "##C0C0C0" }}
                                                name="confirmpassword" placeholder="Enter Name" value={this.state.confirmpassword} onChange={(e) => {this.onChange(e)}}
                                                required />
                                            <Form.Control.Feedback type="invalid">
                                                Confirm Password is required
                                            </Form.Control.Feedback>
                                        </Form.Group>
                                        <br />
                                        <Form.Group className="text-left">
                                            <label> Email </label>
                                            <Form.Control type="email" style={{ backgroundColor: "##C0C0C0" }}
                                                name="email" placeholder="Enter Name" value={this.state.email} onChange={(e) => {this.onChange(e)}}
                                                required />
                                            <Form.Control.Feedback type="invalid">
                                                Email is required
                                            </Form.Control.Feedback>
                                        </Form.Group>
                                        <br />
                                        <Form.Group className="text-left">
                                            <label> Mobile Numbeer </label>
                                            <Form.Control type="number" style={{ backgroundColor: "##C0C0C0" }}
                                                name="mobile" placeholder="Enter Name" value={this.state.mobile} onChange={(e) => {this.onChange(e)}}
                                                required />
                                            <Form.Control.Feedback type="invalid">
                                                Mobile Number is required
                                            </Form.Control.Feedback>
                                        </Form.Group>
                                        <br />
                                        {this.state.message}
                                        <br />
                                        <br />
                                        <Button type="submit" variant="primary" onClick={this.signUp}> SignUp </Button>
                                        &nbsp;&nbsp;&nbsp;
                                        <Button type="submit" variant="danger" onClick={this.loginBack}> Cancel </Button>
                                    </div>
                                </Form>
                            </CardContent>
                        </Card>
                    </div>
                    <div className="col-md-3">
                    </div>
                </div>
            </React.Fragment >
        )
    }

}

function mapStateToProps(state, props) {
    return {};
}

export default connect(mapStateToProps)(SignUp);
