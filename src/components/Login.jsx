import React from 'react';
import { connect } from 'react-redux';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import axios from 'axios';
import { login } from '../Actions/loginActions.js';
import '../App.css';
import HeaderMain from './HeaderMain.jsx';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isvalid: false,
            message: '',
            userMatch: false

        }
        this.userNameRef = React.createRef();
        this.passwordRef = React.createRef();
        this.handleLogin = this.handleLogin.bind(this)

    }
    handleLogin(event) {
        event.preventDefault();
        const login = event.currentTarget;
        if (login.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
            this.setState({ message: "" })
            this.setState({ isvalid: true });
        } else {
        }
    }

    signIn = () => {
        let userName = "";
        this.setState({ userMatch: false });
        axios.get('user.json').then(result => {
            for (let i = 0; i < result.data.length; i++) {
                if (result.data[i].username === this.userNameRef.current.value && result.data[i].password === this.passwordRef.current.value) {
                    userName = result.data[i].username;
                    this.setState({ message: "" })
                    this.setState({ userMatch: true });
                    break;
                }
            }
            if (this.state.userMatch) {
                axios.get('products.json').then(resultP => {
                    try {
                        axios.get('cart.json').then(resultC => {
                            let productsInCart = [];
                            resultC.data.forEach(eachUser => {
                                if(eachUser.username === userName && eachUser.statusOfCart === 'Open') {
                                    eachUser.productsInCart.forEach(eahcCartProduct => {
                                        productsInCart.push(eahcCartProduct);
                                    });
                                }
                            });
                            this.props.doLogin(userName, productsInCart, resultP.data);
                        }).catch(error => {
                            this.setState({ message: "Invalid username or password" })
                        });
                    } catch (error) {
                        
                    }
                }).catch(error => {
                });
                this.props.history.push('/home');
            } else {
                this.setState({ message: "Invalid username or password" })
                this.props.history.push('/');
            }
        }).catch(error => {
            this.props.history.push('/');
            this.setState({ message: "Invalid username or password" })
        });
    }

    signUp = () => {
        this.props.history.push('/signup');
    }

    render() {
        const { isvalid } = this.state;
        return (
            <React.Fragment>
                <HeaderMain />
                <div className="row">
                    <div className="col-md-4 col-sm-4 offset-md-4">
                        <Card>
                            <CardContent>
                                <Form noValidate validated={isvalid}
                                    onSubmit={e => this.handleLogin(e)}>
                                    <div className="text-center" style={{ padding: "10px" }}>
                                        <h3> Login </h3><br />
                                        <Form.Group className="text-left">
                                            <label> User Name </label>
                                            <Form.Control type="text" style={{ backgroundColor: "##C0C0C0" }}
                                                name="name" placeholder="Enter Name" ref={this.userNameRef}
                                                required />
                                            <Form.Control.Feedback type="invalid">
                                                User Name is required
                                            </Form.Control.Feedback>
                                        </Form.Group>
                                        <br />
                                        <Form.Group className="text-left">
                                            <label> Password </label>
                                            <Form.Control type="password" style={{ backgroundColor: "##C0C0C0" }}
                                                name="password" placeholder="Enter Password"
                                                required ref={this.passwordRef} />
                                            <Form.Control.Feedback type="invalid">
                                                Password is required
                                            </Form.Control.Feedback>
                                        </Form.Group>
                                        <br />
                                        {this.state.message}
                                        <br/>
                                        <Button type="submit" variant="primary" onClick={this.signIn}> Login </Button>
                                        &nbsp;&nbsp;&nbsp;
                                        <Button type="submit" variant="primary" onClick={this.signUp}> SignUp </Button>
                                    </div>
                                </Form>
                            </CardContent>
                        </Card>
                    </div>
                    <div className="col-md-3">
                    </div>
                </div>
            </React.Fragment >
        )
    }

}

function mapStateToProps(state, props) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return {
        doLogin: (username, productsInCart, products) => {
            dispatch(login(username, productsInCart, products));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
