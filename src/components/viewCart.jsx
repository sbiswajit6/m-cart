import React from 'react';
import { connect } from 'react-redux';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import '../App.css';
import HeaderMain from './HeaderMain.jsx';
import Table from 'react-bootstrap/Table';
import { updateCart } from '../Actions/loginActions.js';
import { withRouter } from 'react-router-dom';

class ViewCart extends React.Component {

    checkOut = () => {
        let checkoutProducts = [];
        this.props.doProductInCartUpdate(checkoutProducts, this.props.username, this.props.products);
        this.props.history.push('/home');
    }

    render() {
        let displayCard;
        let tableRow = [];
        let totalPrice = 0;
        if (this.props.productsInCart && this.props.productsInCart.length > 0) {
            this.props.productsInCart.forEach(element => {
                tableRow.push(
                    <tr key={element.productId}>
                        <td>{element.productName}</td>
                        <td>{element.quantity}</td>
                        <td>&#2352;&nbsp;{element.quantity * element.price}</td>
                    </tr>
                );
                totalPrice = totalPrice + (element.quantity * element.price);
            });
            displayCard = <div className="row">
                <div className="col-md-8 col-sm-8 offset-md-2">
                    <Card>
                        <Card.Header as="h5">My Cart</Card.Header>
                        <Card.Body>
                            <div className="row">
                                <div className="col-md-12 col-sm-12">
                                    <Table responsive>
                                        <thead>
                                            <tr>
                                                <th>Product</th>
                                                <th>Quantity</th>
                                                <th>Total Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {tableRow}
                                        </tbody>
                                    </Table>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-8 col-sm-8 text-right">
                                    <b> Final Price :&nbsp;&nbsp;&nbsp; &#2352;&nbsp;{totalPrice} </b>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12 col-sm-12 text-right">
                                <Button variant="primary" onClick={ this.checkOut}>Check Out</Button>
                                </div>
                            </div>
                        </Card.Body>
                    </Card>
                </div>
                <div className="col-md-2 col-sm-2"></div>
            </div>;
        } else {
            displayCard = <div className="row">
                <div className="col-md-12 col-sm-12 text-center">
                    No Product for checkout
            </div> </div>;
        }
        return (
            <React.Fragment>
                <HeaderMain /><br />
                {displayCard}
            </React.Fragment>
        )
    }
}

function mapStateToProps(state, props) {
    return {
        productsInCart: state.loginLogoutReducer.productsInCart,
        username: state.loginLogoutReducer.username,
        products: state.loginLogoutReducer.products
    };
}

function mapDispatchToProps(dispatch) {
    return {
        doProductInCartUpdate: (productsInCart, username, products) => {
            dispatch(updateCart(productsInCart, username, products));
        }
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ViewCart));