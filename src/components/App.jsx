import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Login from './Login.jsx';
import SignUp from './signup.jsx';
import Home from './home.jsx';
import ViewProductDetail from './viewProductDetail.jsx';
import ViewCart from './viewCart.jsx';

import { Provider } from 'react-redux';
import store from '../Stores/store.js';

class App extends React.Component {
    render() {
        return (<Provider store={store}>
            <Router>
                <div>
                    <Route exact path="/" component={Login} />
                    <Route exact path="/logout" component={Login} />
                    <Route path="/home" component={Home} />
                    <Route path="/viewproduct/:productid" component={ViewProductDetail} />
                    <Route path="/viewcart" component={ViewCart} />
                    <Route path="/signup" component={SignUp} />
                </div>
            </Router>
        </Provider>)
    }
}

export default App;



