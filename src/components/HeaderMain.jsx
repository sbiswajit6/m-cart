import { connect } from 'react-redux';
import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Form from 'react-bootstrap/Form';
import Image from 'react-bootstrap/Image';
import Button from 'react-bootstrap/Button';
import '../App.css';
import { logout } from '../Actions/loginActions.js';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';

class HeaderMain extends React.Component {

    logOut = () => {
        this.props.doLogout();
        this.props.history.push('/');
    }

    render() {
        let showWelComeDiv;
        let showCartDiv;
        let button;
        if(this.props.username) {
            showWelComeDiv = <div className="mr-sm-2"> Welcome, {this.props.username}&nbsp;&nbsp;&nbsp;</div>;
            button = <Button variant="primary" onClick={this.logOut}>Logout</Button>;
        }
        if(this.props.productsInCart && this.props.productsInCart.length > 0) {
            let totalCartPrice = 0;
            let cartItem = 0;
            this.props.productsInCart.forEach(each => {
                cartItem = cartItem + each.quantity;
                totalCartPrice = totalCartPrice + (each.quantity * each.price);
            });
            showCartDiv = <div> <Image src="../shopping-cart.png" className="log-cart"/>&nbsp;&nbsp;<Link to="/viewcart">{cartItem} item(s), &#2352;&nbsp;{totalCartPrice}&nbsp;&nbsp;&nbsp;</Link></div>
        }
        return (
            <React.Fragment>
                <Navbar bg="light" variant="light">
                    <Navbar.Brand>mCart&nbsp;&nbsp;<Image src="../shopping-cart.png" className="log-cart"/> </Navbar.Brand>
                    <Nav className="mr-auto">
                    </Nav>
                    <Form className="header-color" inline>
                        {showWelComeDiv}{showCartDiv}{button}
                        
                    </Form>
                </Navbar>
            </React.Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        username: state.loginLogoutReducer.username,
        productsInCart: state.loginLogoutReducer.productsInCart
    }
}

function mapDispatchToProps(dispatch) {
    return {
        doLogout: () => {
            dispatch(logout());
        }
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HeaderMain));