import React from 'react';
import { connect } from 'react-redux';
import { updateCart } from '../Actions/loginActions.js';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import '../App.css';
import Image from 'react-bootstrap/Image';
import Rating from '@material-ui/lab/Rating';
import HeaderMain from './HeaderMain.jsx';
import { withRouter } from 'react-router-dom';

class ViewProductDetail extends React.Component {

    constructor() {
        super();
        this.state = {
            productSelected: {
                rating: 0
            }
        };
    }

    addToCart = (product) => {
        let updated = false;
        let productsInCart = this.props.productsInCart;
        productsInCart.forEach(eachProduct => {
            if (eachProduct.productId === product.productId) {
                eachProduct.quantity = eachProduct.quantity + 1;
                updated = true;
            }
        });
        if (!updated) {
            let pr = {};
            pr.productId = product.productId;
            pr.productName = product.productName;
            pr.quantity = 1;
            pr.price = product.price;
            productsInCart.push(pr);
        }
        this.props.doCartUpdate(productsInCart, this.props.username, this.props.products);
        setTimeout(() => {
            this.forceUpdate();
        }, 1);
    }

    componentDidMount() {
        this.props.products.forEach(eachProduct => {
            if (eachProduct.productId === parseInt(this.props.match.params.productid)) {
                this.setState({ productSelected: eachProduct });
            }
        });
    }

    back = () => {
        this.props.history.push('/home');
    }

    render() {
        let displayCard;
        if (this.state.productSelected) {
            displayCard = <div className="row">
                <div className="col-md-8 col-sm-8 offset-md-2">
                    <Card>
                        <Card.Header as="h5">{this.state.productSelected.productName}</Card.Header>
                        <Card.Body>
                            <div className="row">
                                <div className="col-md-4 col-sm-4">
                                    <Image variant="top" className="product-img" src={`../${this.state.productSelected.imageUrl}`} />
                                </div>
                                <div className="col-md-8 col-sm-8">
                                    <Card.Title>By {this.state.productSelected.manufacturer}<br /></Card.Title>
                                    <Card.Text>
                                        <Rating name="read-only" value={this.state.productSelected.rating} readOnly />
                                        <br />
                                        Price : &#2352;&nbsp;{this.state.productSelected.price}<br />
                                        Description : {this.state.productSelected.description} <br />
                                        <Button variant="outline-primary" onClick={(e) => { this.addToCart(this.state.productSelected) }}>Add to Cart</Button>
                                    </Card.Text>
                                </div>
                            </div>
                        </Card.Body>
                    </Card>
                </div>
                <div className="col-md-2 col-sm-2"></div>
            </div>;
        } else {
            displayCard = <div className="row">
                <div className="col-md-12 col-sm-12 text-center">
                    No Product forund
            </div> </div>;
        }
        return (
            <React.Fragment>
                <HeaderMain />
                <br />
                <div className="row">
                    <div className="col-md-11 col-sm-11 text-right">
                        <Button onClick={this.back}> Go Back</Button>
                    </div>
                </div>
                {displayCard}
            </React.Fragment>
        )

    }
}

function mapStateToProps(state, props) {
    return {
        productsInCart: state.loginLogoutReducer.productsInCart,
        products: state.loginLogoutReducer.products,
        username: state.loginLogoutReducer.username
    };
}

function mapDispatchToProps(dispatch) {
    return {
        doCartUpdate: (productsInCart, username, products) => {
            dispatch(updateCart(productsInCart, username, products));
        }
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ViewProductDetail));