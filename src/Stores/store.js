import {applyMiddleware,createStore} from 'redux';
import appReducer from '../Reducers/appReducer';

import thunk from 'redux-thunk';


var store= applyMiddleware(thunk)(createStore)(appReducer);
export default store;
