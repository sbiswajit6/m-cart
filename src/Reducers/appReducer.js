import { combineReducers } from 'redux'
import loginLogoutReducer from './loginLogoutReducer.js';


const appReducer = combineReducers({
    loginLogoutReducer
})

export default appReducer;