import initialState from './initialState';

const loginLogoutReducer = (state = initialState, action) => {

	switch (action.type) {
		case 'LOGIN':
			return Object.assign({}, state, { username: action.username, productsInCart: action.productsInCart, products: action.products });
		case 'LOGOUT':
			return Object.assign({}, state, { username: "", productsInCart: [], products: [] });
		case 'UPDATE_CART':
			return Object.assign({}, state, { username: action.username, productsInCart: action.productsInCart, products: action.products });
		default:
			return state;
	}
};

export default loginLogoutReducer;
