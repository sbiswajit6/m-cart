export function login(username, productsInCart, products){ 
	return 	{type: "LOGIN", username: username, productsInCart: productsInCart, products: products}
}

export function logout(){
	return 	{type: "LOGOUT"}
}

export function updateCart(productsInCart, username, products){ 
	return 	{type: "UPDATE_CART",  username: username, productsInCart: productsInCart, products: products}
}
